package com.agp.ibapi.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.agp.ibapi.mapper.CustomerAccountMapper;

@Repository
public class CustomerAccountRepository {

	@Autowired
	private CustomerAccountMapper mapper;


	public List<Map<String, Object>> getAllCustomers() {
		return mapper.getAllCustomers();
	}
	

}
