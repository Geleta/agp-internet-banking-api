package com.agp.ibapi.repository;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agp.ibapi.dto.AccountBalance;
import com.agp.ibapi.dto.AccountTransaction;
import com.agp.ibapi.dto.BankBranch;
import com.agp.ibapi.dto.BankRegion;
import com.agp.ibapi.dto.CustomerAccount;
import com.agp.ibapi.dto.LastClosingBalance;
import com.agp.ibapi.mapper.BankLocationMapper;


@Repository
public class BankLocationRepository {

	@Autowired
	private BankLocationMapper locationMapper;
	


	public List<BankRegion> getAllRegions() {
		return locationMapper.getAllRegions();
	}
	
	public List<BankBranch> getAllBranches() {
		return locationMapper.getAllBranchs();
	}
	
	public List<BankBranch> getBranchsByRegion(Map<String, Object> filterParams) {
		List<BankBranch> a= locationMapper.getBranchsByRegion(filterParams);
		return a;
	}
	
	public LastClosingBalance getLastClosingBalance(Map<String,Object> filterParams){
		return locationMapper.getLastClosingBalance(filterParams);
	}
	//
	public List<AccountTransaction> getTransactionByDate(Map<String,Object> filterParams){
		return locationMapper.getTransactionByDate(filterParams);
	}
	
	//
	public List<AccountTransaction> getNTransaction(Map<String,Object> filterParams){
		return locationMapper.getNTransaction(filterParams);
	}
	
	
	public List<AccountBalance> getAccountBalance(Map<String,Object> filterParams){
		return locationMapper.getAccountBalance(filterParams);
	}
	
	//getCustomerAccount
	public List<CustomerAccount> getCustomerAccount(Map<String,Object> filterParams){
		return locationMapper.getCustomerAccount(filterParams);
	}
	
	//getTransactionByDateAsc
	public List<AccountTransaction> getTransactionByDateAsc(Map<String,Object> filterParams){
		return locationMapper.getTransactionByDateAsc(filterParams);
	}

	
	
}
