package com.agp.ibapi.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.agp.ibapi.dto.AccountBalance;
import com.agp.ibapi.dto.AccountBalanceSummary;
import com.agp.ibapi.dto.AccountSummary;
import com.agp.ibapi.dto.AccountTransaction;
import com.agp.ibapi.dto.BankBranch;
import com.agp.ibapi.dto.BankRegion;
import com.agp.ibapi.dto.CustomerAccount;
import com.agp.ibapi.dto.LastClosingBalance;
import com.agp.ibapi.repository.BankLocationRepository;

@Service
public class BankLocationService {

	@Autowired
	BankLocationRepository locationRepo;
	
	@Autowired
    private RestTemplate restTemplate;
	
	public List<BankRegion> getAllRegions() {
		return locationRepo.getAllRegions();
		
	}
	
	public List<BankBranch> getAllBranches() {
		return locationRepo.getAllBranches();
		
	}
	
	public List<BankBranch> getBranchsByRegion(Map<String, Object> filterParams) {
			return locationRepo.getBranchsByRegion(filterParams);
	}
	
	public List<BankBranch> getBranchsByRegionRest(Map<String, Object> filterParams) {
		
		String urlparms="region="+"ASASA";
		String url="http://localhost:8080/api/searchBranch?"
				+ urlparms;
		
		ResponseEntity<List<BankBranch>> searchBranchApiResponse =
		        restTemplate.exchange(url,
		                    HttpMethod.GET, null, new ParameterizedTypeReference<List<BankBranch>>() {
		            });
		List<BankBranch> branchList = searchBranchApiResponse.getBody();
		return branchList;
    }
	
	 
 
	public LastClosingBalance getLastClosingBalance(Map<String,Object> filterParams){
		return locationRepo.getLastClosingBalance(filterParams);
	}
	
	public List<AccountTransaction> getTransactionByDateAsc(Map<String,Object> filterParams){
		return locationRepo.getTransactionByDateAsc(filterParams);
	}
	
	public List<AccountTransaction> getTransactionByDate(Map<String,Object> filterParams){
		return locationRepo.getTransactionByDate(filterParams);
	}
	
	public List<AccountSummary> getAccountSummary(Map<String,Object> filterParams){
		
		LastClosingBalance lastBalance= this.getLastClosingBalance(filterParams);
		
		List<AccountTransaction> transactions= this.getTransactionByDateAsc(filterParams);
		//transactions.sort((o1,o2) -> o1.getFundEffectDate().compareTo(o2.getFundEffectDate()));
		List<AccountSummary> summaries=new ArrayList<AccountSummary>();
		Double openingBalance=0.0;
		if(lastBalance!=null ) {
		 openingBalance=lastBalance.getBalance();
		}
		Integer id=1;
		
		for(AccountTransaction accTnx : transactions) {
			
			Double closingBalance=(double) 0;
			AccountSummary summary=new AccountSummary();
			
			summary.setId(id);
			summary.setAccountNumber(accTnx.getAccountNumber());
			summary.setBranchCode(accTnx.getBranchCode());
			summary.setOpeningBalance(openingBalance);
			summary.setCredit(accTnx.getCredit());
			summary.setDebit(accTnx.getDebit());
			summary.setDebitOrCredit(accTnx.getDebitOrCredit());
			summary.setEntryDate(accTnx.getEntryDate());
			summary.setPostDate(accTnx.getPostDate());
			summary.setFundEffectDate(accTnx.getFundEffectDate());
			summary.setNote(accTnx.getNote());
			summary.setInstrumentNumber(accTnx.getInstrumentNumber());
			
	 
			closingBalance=openingBalance + accTnx.getCredit() - accTnx.getDebit();
			summary.setClosingBalance(closingBalance);
			openingBalance=closingBalance;
			
			id++;
			summaries.add(summary);
		
			
		}
		//summaries.sort((o2,o1) -> o1.getId().compareTo(o2.getId()));
		return summaries;
	}
	
	//
	public List<AccountTransaction> getNTransaction(Map<String,Object> filterParams){
		return locationRepo.getNTransaction(filterParams);
	}
	
	
	public List<AccountBalance> getAccountBalance(Map<String,Object> filterParams){
		return locationRepo.getAccountBalance(filterParams);
	}
	
	public AccountBalanceSummary getAccountBalanceSummary(Map<String,Object> filterParams){
		
		AccountBalance accBalance=new AccountBalance();
		AccountTransaction accTnx=new AccountTransaction();
		
		if(this.getAccountBalance(filterParams).size()>0) {
			accBalance=this.getAccountBalance(filterParams).get(0);
		}//.get(0);
		
		if(this.getNTransaction(filterParams).size()>0) {
			accTnx=this.getNTransaction(filterParams).get(0);
		}
		
		AccountBalanceSummary accBalanceSummary=new AccountBalanceSummary();
		
		accBalanceSummary.customerNumber=accBalance.customerNumber;
		accBalanceSummary.accountNumber=accBalance.accountNumber;
		accBalanceSummary.branchCode=accBalance.branchCode;
		accBalanceSummary.branchName=accBalance.branchName;
		accBalanceSummary.clearedBalance=accBalance.clearedBalance;
		accBalanceSummary.unclearedBalance=accBalance.unclearedBalance;
		accBalanceSummary.accountName=accBalance.accountName;
		accBalanceSummary.formattedAccountNumber=accBalance.formattedAccountNumber;
		
		accBalanceSummary.amount=accTnx.amount;
		accBalanceSummary.credit=accTnx.credit;
		accBalanceSummary.debit=accTnx.debit;
		accBalanceSummary.debitOrCredit=accTnx.debitOrCredit;
		accBalanceSummary.entryDate=accTnx.entryDate;
		accBalanceSummary.postDate=accTnx.postDate;
		accBalanceSummary.fundEffectDate=accTnx.fundEffectDate;
		accBalanceSummary.note=accTnx.note;
		
		
		
		return accBalanceSummary;
	
	}
	
	//getCustomerAccount
	public List<CustomerAccount> getCustomerAccount(Map<String,Object> filterParams){
		return locationRepo.getCustomerAccount(filterParams);
	}

}
