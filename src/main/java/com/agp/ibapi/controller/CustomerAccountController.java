package com.agp.ibapi.controller;



import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.agp.ibapi.dto.AccountBalanceSummary;
import com.agp.ibapi.dto.AccountSummary;
import com.agp.ibapi.dto.AccountTransaction;
import com.agp.ibapi.dto.BankBranch;
import com.agp.ibapi.dto.BankRegion;
import com.agp.ibapi.dto.CustomerAccount;
import com.agp.ibapi.dto.LastClosingBalance;
import com.agp.ibapi.service.BankLocationService;
import com.agp.ibapi.service.CustomerAccountService;


@RestController
@RequestMapping("/api")
public class CustomerAccountController {

	
	@Autowired
	CustomerAccountService custService;
	
	@Autowired
	BankLocationService locationService;
	
	
	
	// Get All Notes
	@ResponseBody
	@GetMapping("/AllCustomers")
	public List<Map<String,Object>> getAllCustomer() {
	    return custService.getAllCustomer();
	}
	
	// Get All Region
		//@ResponseBody
		@GetMapping("/AllRegions")
		public List<BankRegion> getAllRegions() {
		    return locationService.getAllRegions();
		}
		
		@ResponseBody
		@RequestMapping(value = "/searchBranch", method = RequestMethod.GET)
		public List<BankBranch> getBranchsByRegion(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getBranchsByRegion(filterParams);
		}
		
		//getBranchsByRegionRest
		@ResponseBody
		@RequestMapping(value = "/getBranchsByRegionRest", method = RequestMethod.GET)
		public List<BankBranch> getBranchsByRegionRest(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getBranchsByRegionRest(filterParams);
		}
		
		@ResponseBody
		@RequestMapping(value="/getLastClosingBalance", method=RequestMethod.GET)
		public LastClosingBalance getLastClosingBalance(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getLastClosingBalance(filterParams);
		}
		//
		
		@ResponseBody
		@RequestMapping(value="/getTransactionByDate", method=RequestMethod.GET)
		public List<AccountTransaction> getTransactionByDate(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getTransactionByDate(filterParams);
		}
		
		//
		@ResponseBody
		@RequestMapping(value="/getAccountSummary", method=RequestMethod.GET)
		public List<AccountSummary> getAccountSummary(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getAccountSummary(filterParams);
		}
		
		//
		@ResponseBody
		@RequestMapping(value="/getNTransaction", method=RequestMethod.GET)
		public List<AccountTransaction> getNTransaction(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getNTransaction(filterParams);
		}
		
		//getAccountBalanceSummary
		@ResponseBody
		@RequestMapping(value="/getAccountBalanceSummary", method=RequestMethod.GET)
		public AccountBalanceSummary getAccountBalanceSummary(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getAccountBalanceSummary(filterParams);
		}
		
		//getCustomerAccount
		@ResponseBody
		@RequestMapping(value="/getCustomerAccount", method=RequestMethod.GET)
		public List<CustomerAccount> getCustomerAccount(@RequestParam Map <String,Object> filterParams, HttpServletRequest request) throws InterruptedException {
			return locationService.getCustomerAccount(filterParams);
		}
	
	
}
