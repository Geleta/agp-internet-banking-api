package com.agp.ibapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonSerialize()
@JsonInclude(Include.NON_NULL)
@Data
public class BankRegion {

	//private Integer id;
	private String branchCode;
	private String branchName;
	private String regionName ;
	
}



