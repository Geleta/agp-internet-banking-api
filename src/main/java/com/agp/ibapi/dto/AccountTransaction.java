package com.agp.ibapi.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonSerialize()
@JsonInclude(Include.NON_NULL)
@Data
public class AccountTransaction {

	public String customerNumber;
	public String branchCode;
	public String accountNumber;
	public Date entryDate;
	public Date postDate;
	public Date fundEffectDate;
	public String debitOrCredit;
	public Double credit; 
	public Double debit;
	public Double amount;
	public String note;
	public String instrumentNumber;
	public String formattedAccountNumber;
}
