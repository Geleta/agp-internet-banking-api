package com.agp.ibapi.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonSerialize()
@JsonInclude(Include.NON_NULL)
@Data
public class AccountBalanceSummary {

	public String customerNumber;
	public String branchCode;
	public String branchName;
	public String accountNumber;
	public Double clearedBalance;
	public Double unclearedBalance;
	public String accountName;
	public String formattedAccountNumber;
	
	public Date entryDate;
	public Date postDate;
	public Date fundEffectDate;
	public String debitOrCredit;
	public Double credit; 
	public Double debit;
	public Double amount;
	public String note;
}
