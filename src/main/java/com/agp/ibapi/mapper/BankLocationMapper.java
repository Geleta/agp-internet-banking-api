package com.agp.ibapi.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import com.agp.ibapi.dto.AccountBalance;
import com.agp.ibapi.dto.AccountTransaction;
import com.agp.ibapi.dto.BankBranch;
import com.agp.ibapi.dto.BankRegion;
import com.agp.ibapi.dto.CustomerAccount;
import com.agp.ibapi.dto.LastClosingBalance;

@Mapper
public interface BankLocationMapper {

	@Select(" SELECT  DISTINCT ADD3 AS RegionName FROM D001003 ORDER BY ADD3 ")
	List<BankRegion> getAllRegions();
	
	
	@Select(" SELECT PBRCODE AS BranchCode,NAME AS BranchName,ADD3 AS RegionName FROM D001003 ")
	List<BankBranch> getAllBranchs();
	
	@SelectProvider(type=ApiQueryBuilder.class, method="getBranchByRegionQuery")
	List<BankBranch> getBranchsByRegion(Map<String, Object> filterParams);
	
	
	@SelectProvider(type=ApiQueryBuilder.class, method="getLastClosingBalanceQuery")
	LastClosingBalance getLastClosingBalance(Map<String, Object> filterParams);
	
	
	@SelectProvider(type=ApiQueryBuilder.class, method="getTransactionByDate")
	List<AccountTransaction> getTransactionByDate(Map<String, Object> filterParams);
	
	@SelectProvider(type=ApiQueryBuilder.class, method="getNTransaction")
	List<AccountTransaction> getNTransaction(Map<String, Object> filterParams);
	
	//getAccountBalance
	@SelectProvider(type=ApiQueryBuilder.class, method="getAccountBalance")
	List<AccountBalance> getAccountBalance(Map<String, Object> filterParams);
	
	//getCustomerAccount
	@SelectProvider(type=ApiQueryBuilder.class, method="getCustomerAccount")
	List<CustomerAccount> getCustomerAccount(Map<String, Object> filterParams);
	
	//getTransactionByDateAsc
	@SelectProvider(type=ApiQueryBuilder.class, method="getTransactionByDateAsc")
	List<AccountTransaction> getTransactionByDateAsc(Map<String, Object> filterParams);
	
}
