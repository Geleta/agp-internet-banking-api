package com.agp.ibapi.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


@Mapper
public interface CustomerAccountMapper {

	
	@Select(" select * from d009326 ")
	List<Map<String, Object>> getAllCustomers();

}
