package com.agp.ibapi.mapper;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;


public class ApiQueryBuilder {
	
	public static String getBranchByRegionQuery(Map params) {
		
		String sql = new SQL() {{
			
			SELECT("  PBRCODE AS BranchCode,NAME AS BranchName,ADD3 AS RegionName ");
			FROM(" D001003 ");
			if (params.containsKey("region") && !params.get("region").toString().isEmpty()) {
		    	WHERE(" ADD3='"+params.get("region").toString()+"'");
		    }
		}}.toString();
		
		return sql;
	}
	
	
	public static String getLastClosingBalanceQuery(Map params) {
		
		
		String sql = new SQL() {{
			SELECT(" * FROM ( SELECT "
					+ " LBRCODE AS BranchCode,PRDACCTID AS AccountNumber,"
					+ "CBLDATE AS LastBalanceClosingDate,BALANCE4 AS Balance ");
			FROM(" D010014 ");
			if (params.containsKey("accountnumber") && !params.get("accountnumber").toString().isEmpty()) {
		    	WHERE(" PRDACCTID='"+params.get("accountnumber").toString()+"'");
		    }
			if (params.containsKey("branchcode") && !params.get("branchcode").toString().isEmpty()) {
		    	WHERE(" LBRCODE='"+params.get("branchcode").toString()+"'");
		    }
			if (params.containsKey("fromdate") && !params.get("fromdate").toString().isEmpty()) {
				
		    	WHERE(" CBLDATE <= TO_DATE( '"+ params.get("fromdate")+"', 'YYYY-MM-DD')");
		    }
			
			//
			ORDER_BY("cbldate DESC )  WHERE rownum=1" );
		}}.toString();
		
		return sql;
	}
	
	
	public static String getTransactionByDate(Map params) {
		
		String sql =new SQL() {{
			SELECT(" LBRCODE AS BranchCode,vcracctid AS AccountNumber,entrydate AS EntryDate ");
			SELECT(" postdate AS PostDate,feffdate AS FundEffectDate, drcr AS DebitOrCredit ");
			SELECT(" (CASE drcr WHEN 'C' THEN lcytrnamt ELSE 0 END) AS Credit ");
			SELECT(" (CASE drcr WHEN 'D' THEN lcytrnamt ELSE 0 END) AS Debit ");
			SELECT(" LCYTRNAMT AS Amount,PARTICULARS AS Note ");
			SELECT(" INSTRNO AS InstrumentNumber ");
			FROM (" D009040 ");
			WHERE(" D009040.SHTOTFLAG='Y' AND D009040.SHCLRFLAG='Y' AND D009040.ACTOTFLAG='Y' AND D009040.ACCLRFLAG='Y' AND "
					+ " D009040.POSTFLAG='P' AND D009040.AUTHFLAG='A' AND D009040.FEFFFLAG='F' AND D009040.CANCELEDFLAG <> 'C' AND "
					+ " D009040.NOAUTHOVER=1 AND D009040.NOAUTHPENDING=0 ");
			
			if (params.containsKey("accountnumber") && !params.get("accountnumber").toString().isEmpty()) {
		    	WHERE(" vcracctid='"+params.get("accountnumber").toString()+"'");
		    }
			if (params.containsKey("branchcode") && !params.get("branchcode").toString().isEmpty()) {
		    	WHERE(" LBRCODE='"+params.get("branchcode").toString()+"'");
		    }
			//WHERE(" CBLDATE <= TO_DATE( '"+ params.get("fromdate")+"', 'YYYY-MM-DD')");
			if (params.containsKey("fromdate") && !params.get("fromdate").toString().isEmpty() &&
					params.containsKey("todate") && !params.get("todate").toString().isEmpty()
					) {
		    	WHERE(" D009040.FEFFDATE  BETWEEN TO_DATE('"+params.get("fromdate")
		    			+"', 'YYYY-MM-DD') AND TO_DATE('"
		    			+params.get("todate")
		    			+"', 'YYYY-MM-DD') ");
		    }
			ORDER_BY(" entrydate DESC,D009040.FEFFDATE DESC,posttime DESC ");
			
		}}.toString();
		return sql;
	}
	
	public static String getNTransaction(Map params) {
		
		String sql =new SQL() {{
			SELECT(" * FROM ( SELECT "
					+ " LBRCODE AS BranchCode,vcracctid AS AccountNumber,entrydate AS EntryDate ");
			SELECT(" postdate AS PostDate,feffdate AS FundEffectDate, drcr AS DebitOrCredit ");
			SELECT(" (CASE drcr WHEN 'C' THEN lcytrnamt ELSE 0 END) AS Credit ");
			SELECT(" (CASE drcr WHEN 'D' THEN lcytrnamt ELSE 0 END) AS Debit ");
			SELECT(" LCYTRNAMT AS Amount,PARTICULARS AS Note ");
			SELECT(" (D001003.NAME||'-'||TRIM(LEADING '0' FROM (substr(vcracctid,9,8)))||'/'||(substr(vcracctid,1,7)) "
					+ " ||'/'||TRIM(LEADING '0' FROM (substr(vcracctid,17,8)))) AS FormattedAccountNumber ");
			//SELECT(" (CASE drcr WHEN 'C' THEN lcytrnamt ELSE 0 END) AS Credit ");
		    //SELECT(" (CASE drcr WHEN 'D' THEN lcytrnamt ELSE 0 END) AS Debit ");
			//SELECT(" LCYTRNAMT AS Amount,PARTICULARS AS Note ");
			FROM (" D009040 INNER JOIN  D001003 ON D001003.PBRCODE=D009040.LBRCODE ");
			WHERE(" D009040.SHTOTFLAG='Y' AND D009040.SHCLRFLAG='Y' AND D009040.ACTOTFLAG='Y' AND D009040.ACCLRFLAG='Y' AND "
					+ " D009040.POSTFLAG='P' AND D009040.AUTHFLAG='A' AND D009040.FEFFFLAG='F' AND D009040.CANCELEDFLAG <> 'C' AND "
					+ " D009040.NOAUTHOVER=1 AND D009040.NOAUTHPENDING=0 ");
			
			if (params.containsKey("accountnumber") && !params.get("accountnumber").toString().isEmpty()) {
		    	WHERE(" vcracctid='"+params.get("accountnumber").toString()+"'");
		    }
			if (params.containsKey("branchcode") && !params.get("branchcode").toString().isEmpty()) {
		    	WHERE(" LBRCODE='"+params.get("branchcode").toString()+"'");
		    }
			
			if (params.containsKey("num") && !params.get("num").toString().isEmpty()) {
				ORDER_BY(" entrydate DESC,D009040.FEFFDATE DESC,posttime DESC ) WHERE rownum <= "+params.get("num").toString());
			}
			else{
				ORDER_BY(" entrydate DESC,D009040.FEFFDATE DESC,posttime DESC ) WHERE  rownum=1" );
			}
			
			
		}}.toString();
		return sql;
	}
	
	public static String getAccountBalance(Map params) {
		
		String sql = new SQL() {{
			SELECT(" D009022.CUSTNO AS CustomerNumber");
			SELECT(" D001003.PBRCODE AS BranchCode, D001003.NAME AS BranchName ");
			SELECT(" D009022.PRDACCTID AS AccountNumber, D009022.ACTTOTBALLCY AS ClearedBalance ");
			SELECT(" D009022.SHDTOTBALFCY AS UnclearedBalance ");
			SELECT(" D009022.LONGNAME AS AccountName ");
			SELECT(" (TRIM(LEADING '0' FROM (substr(D009022.PRDACCTID,9,8)))||'/'||(substr(D009022.PRDACCTID,1,7)) "
					+ " ||'/'||TRIM(LEADING '0' FROM (substr(D009022.PRDACCTID,17,8)))) AS FormattedAccountNumber ");
			//SELECT(" D009011.ADD1 AS Address,D009011.PHONE AS Phone ");
			FROM(" D009011 INNER JOIN D009022 ON D009011.CUSTNO=D009022.CUSTNO " + 
				 " INNER JOIN D001003 ON D001003.PBRCODE=D009022.LBRCODE ");
			if (params.containsKey("accountnumber") && !params.get("accountnumber").toString().isEmpty()) {
		    	WHERE(" D009022.PRDACCTID='"+params.get("accountnumber").toString()+"'");
		    }
			if (params.containsKey("branchcode") && !params.get("branchcode").toString().isEmpty()) {
		    	WHERE(" D009022.LBRCODE='"+params.get("branchcode").toString()+"'");
		    }
			
			//
			
		}}.toString();
		
		return sql;
	}
	
	public static String getCustomerAccount(Map params) {
		
		String sql = new SQL() {{
			SELECT(" D009022.CUSTNO AS CustomerNumber,	D009022.PRDACCTID AS AccountNumber ");
			SELECT(" D009022.LONGNAME AS AccountName,D009022.LBRCODE AS BranchCode,D001003.NAME AS BranchName ");
			FROM(" D001003 INNER JOIN D009022 ON D001003.PBRCODE=D009022.LBRCODE ");
			if (params.containsKey("custno") && !params.get("custno").toString().isEmpty()) {
		    	WHERE("  D009022.CUSTNO ='"+params.get("custno").toString()+"'");
		    }
			if (params.containsKey("accountnumber") && !params.get("accountnumber").toString().isEmpty()) {
		    	WHERE(" D009022.PRDACCTID ='"+params.get("accountnumber").toString()+"'");
		    }
			if (params.containsKey("branchcode") && !params.get("branchcode").toString().isEmpty()) {
		    	WHERE("  D009022.LBRCODE ='"+params.get("branchcode").toString()+"'");
		    }
			
			//
			
		}}.toString();
		
		return sql;
	}
	

	public static String getTransactionByDateAsc(Map params) {
		
		String sql =new SQL() {{
			SELECT(" LBRCODE AS BranchCode,vcracctid AS AccountNumber,to_char(entrydate,'YYYY-MM-DD') AS EntryDate ");
			SELECT(" to_char(postdate,'YYYY-MM-DD') AS PostDate,to_char(feffdate,'YYYY-MM-DD') AS FundEffectDate, drcr AS DebitOrCredit ");
			SELECT(" (CASE drcr WHEN 'C' THEN lcytrnamt ELSE 0 END) AS Credit ");
			SELECT(" (CASE drcr WHEN 'D' THEN lcytrnamt ELSE 0 END) AS Debit ");
			SELECT(" LCYTRNAMT AS Amount,PARTICULARS AS Note ");
			SELECT(" INSTRNO AS InstrumentNumber ");
			FROM (" D009040 ");
			WHERE(" D009040.SHTOTFLAG='Y' AND D009040.SHCLRFLAG='Y' AND D009040.ACTOTFLAG='Y' AND D009040.ACCLRFLAG='Y' AND "
					+ " D009040.POSTFLAG='P' AND D009040.AUTHFLAG='A' AND D009040.FEFFFLAG='F' AND D009040.CANCELEDFLAG <> 'C' AND "
					+ " D009040.NOAUTHOVER=1 AND D009040.NOAUTHPENDING=0 ");
			
			if (params.containsKey("accountnumber") && !params.get("accountnumber").toString().isEmpty()) {
		    	WHERE(" vcracctid='"+params.get("accountnumber").toString()+"'");
		    }
			if (params.containsKey("branchcode") && !params.get("branchcode").toString().isEmpty()) {
		    	WHERE(" LBRCODE='"+params.get("branchcode").toString()+"'");
		    }
			//WHERE(" CBLDATE <= TO_DATE( '"+ params.get("fromdate")+"', 'YYYY-MM-DD')");
			if (params.containsKey("fromdate") && !params.get("fromdate").toString().isEmpty() &&
					params.containsKey("todate") && !params.get("todate").toString().isEmpty()
					) {
		    	WHERE(" D009040.FEFFDATE  BETWEEN TO_DATE('"+params.get("fromdate")
		    			+"', 'YYYY-MM-DD') AND TO_DATE('"
		    			+params.get("todate")
		    			+"', 'YYYY-MM-DD') ");
		    }
			ORDER_BY(" entrydate ASC,D009040.FEFFDATE ASC,posttime ASC ");
			
		}}.toString();
		return sql;
	}

	

}
